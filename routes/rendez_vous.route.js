/** @format */

const express = require('express');
const router = express.Router();
const { authJwt } = require('../middlewares');
const {
  Create_rendezvous,
  Get_rendezvous,
  Edit_rendezvous,
  Delete_rendezvous,
  Get_all_rendezvous,
} = require('../controllers/rendez_vous.controller');

router.post('/api/rendez_vous/create', authJwt.verifyToken, Create_rendezvous);

router.put('/api/rendez_vous/:id', authJwt.verifyToken, Edit_rendezvous);

router.delete('/api/rendez_vous/:id', authJwt.verifyToken, Delete_rendezvous);

router.get('/api/rendez_vous/user/:id', authJwt.verifyToken, Get_rendezvous);

module.exports = router;
