/** @format */

const express = require('express');
const router = express.Router();
const { authJwt } = require('../middlewares');

const {
  Delete_letter,
  Edit_letter,
  Get_letter,
  Create_letter,
} = require('../controllers/letter.controller');

router.delete('/api/lettre/:id', authJwt.verifyToken, Delete_letter);

router.post('/api/lettre/create', authJwt.verifyToken, Create_letter);

router.put('/api/lettre/:id', authJwt.verifyToken, Edit_letter);

router.get('/api/lettre/user/:id', authJwt.verifyToken, Get_letter);
module.exports = router;
