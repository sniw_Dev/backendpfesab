/** @format */

const express = require('express');
const router = express.Router();
const { imageUpload } = require('../middlewares');
const path = require('path');

router.post('/api/image', imageUpload.single('image'), (req, res) => {
  const filepath = req.file.path.split('\\')[1];
  console.log(filepath);

  res.json(filepath);
});
// Image Get Routes
router.get('/api/image/:filename', (req, res) => {
  const { filename } = req.params;
  const dirname = path.resolve();
  const fullfilepath = path.join(dirname, 'uploads/' + filename);
  return res.sendFile(fullfilepath);
});

module.exports = router;
