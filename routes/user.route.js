/** @format */

const express = require('express');
const router = express.Router();
const { authJwt } = require('../middlewares');
const { verifySignUp } = require('../middlewares');
const { imageUpload } = require('../middlewares');

const {
  Inscription,
  Authentification,
  GetUser,
  updateUser,
  deleteUser,
} = require('../controllers/user.controller');

router.post(
  '/api/user/inscription',
  // verifySignUp.checkDuplicateEmail,
  imageUpload.single('image'),
  Inscription
);
router.post('/api/user/authentification', Authentification);
router.put('/api/user/update', authJwt.verifyToken, updateUser);
router.delete('/api/user/delete', authJwt.verifyToken, deleteUser);

router.get('/api/user', authJwt.verifyToken, GetUser);
module.exports = router;
