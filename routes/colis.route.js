/** @format */

const express = require('express');
const router = express.Router();
const { authJwt } = require('../middlewares');
const {
  Create_colis,
  Get_colis,
  Edit_colis,
  Delete_colis,
} = require('../controllers/colis.controller');

router.post('/api/colis/create', authJwt.verifyToken, Create_colis);
router.put('/api/colis/:id', authJwt.verifyToken, Edit_colis);
router.delete('/api/colis/:id', authJwt.verifyToken, Delete_colis);
router.get('/api/colis/user/:id', authJwt.verifyToken, Get_colis);

module.exports = router;
