/** @format */
require('dotenv').config();
require('./database/MongooseConnect');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const express = require('express');
const app = express();
const userRouter = require('./routes/user.route');
const lettreRouter = require('./routes/lettre.route');
const colisRouter = require('./routes/colis.route');
const rendezvousRouter = require('./routes/rendez_vous.route');
const imagesRouter = require('./routes/images.route');
const corsConfig = {
  origin: true,
  credentials: true,
};

app.use(cors(corsConfig));

app.use(helmet());
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
/***********importer les routes*********/
app.use(userRouter);
app.use(lettreRouter);
app.use(colisRouter);
app.use(rendezvousRouter);
app.use(imagesRouter);
/**********************************/
app.use(
  session({
    secret: process.env.SESSION_SECRET_KEY,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true },
  })
);
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});

app.listen(process.env.PORT, () => {
  console.log(`Server started on port ${process.env.PORT} 🤖`);
});
