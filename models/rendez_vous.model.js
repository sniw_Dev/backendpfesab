/** @format */

const mongoose = require('mongoose');

const rendezvousSchema = new mongoose.Schema({
  email: {
    type: String,
    trim: true,
  },
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  numero_telephone: {
    type: String,
    trim: true,
  },
  date_rendez_vous: {
    type: Date,
    trim: true,
  },
  temps_rendez_vous: {
    type: String,
    trim: true,
  },
  confirmation: {
    type: String,
    trim: true,
  },
});

const rendezvous = mongoose.model('rendezvous', rendezvousSchema);

module.exports = rendezvous;
