/** @format */

const mongoose = require('mongoose');

const letterSchema = new mongoose.Schema({
  code_lettre: {
    type: String,
    trim: true,
  },
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  adresse_destinataire: {
    type: String,
    trim: true,
  },
  code_postal: {
    type: String,
    trim: true,
  },
  letter_body: {
    type: String,
    trim: true,
  },
  type: {
    type: String,
    trim: true,
  },
});

const letter = mongoose.model('letter', letterSchema);

module.exports = letter;
