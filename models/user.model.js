/** @format */

const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  _id: {
    type: String,
    trim: true,
  },
  nom: {
    type: String,
    trim: true,
  },
  prenom: {
    type: String,
    trim: true,
  },
  email: {
    type: String,
    trim: true,
  },
  password: {
    type: String,
    trim: true,
  },
  address: {
    type: String,
    trim: true,
  },
  codepostal: {
    type: String,
    trim: true,
  },
  image: {
    type: String,
    trim: true,
  },
  genre: {
    type: String,
    trim: true,
  },
});

const User = mongoose.model('User', userSchema);
const validate = (user) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });
  return schema.validate(user);
};

module.exports = { User, validate };
