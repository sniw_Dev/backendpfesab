/** @format */

const mongoose = require('mongoose');

const coliSchema = new mongoose.Schema({
  code_colis: {
    type: String,
    trim: true,
  },
  nom_destinataire: {
    type: String,
    trim: true,
  },
  prenom_destinataire: {
    type: String,
    trim: true,
  },
  numero_destinataire: {
    type: String,
    trim: true,
  },

  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  adresse_destinataire: {
    type: String,
    trim: true,
  },

  description: {
    type: String,
    trim: true,
  },
  poid_colis: {
    type: String,
    trim: true,
  },
  type: {
    type: String,
    trim: true,
  },
});

const colis = mongoose.model('colis', coliSchema);

module.exports = colis;
