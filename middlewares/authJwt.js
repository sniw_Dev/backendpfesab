/** @format */

const jwt = require('jsonwebtoken');
verifyToken = (req, res, next) => {
  let token = req.headers['x-access-token'];
  console.log(token);
  if (!token) {
    return res.status(403).send({ message: 'No token provided!' });
  }
  jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: 'Unauthorized!' });
    }
    console.log(decoded);
    req.userId = decoded._id;
    next();
  });
};
const authJwt = {
  verifyToken,
};
module.exports = authJwt;
