/** @format */

const { User } = require('../models/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const cloudinary = require('cloudinary').v2;
const sharp = require('sharp');
const fs = require('fs');
// const Token = require('../models/token.model');
// const sendEmail = require('../utils/SendEmail');
const crypto = require('crypto');
const Joi = require('joi');

const Inscription = async (req, res) => {
  const filepath = req.file.path.split('\\')[1];

  req.body.image = filepath;
  console.log(filepath);

  //find user
  User.findOne({ email: req.body.email }, (err, user) => {
    if (user) {
      return res.status(409).json({ message: 'this email is registerd!' });
    }
    //hash password
    bcrypt.hash(req.body.password, 10, (err, hash) => {
      //create user
      let expDate = new Date();
      expDate.setDate(expDate.getDate() + 45);
      const userData = new User({
        ...req.body,
        _id: new mongoose.Types.ObjectId(),
        password: hash,
      });
      //save user to db
      userData.save().then(() => {
        return res.status(200).json(userData);
      });
    });
  });
};

const Authentification = async (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (!user) {
      return res.status(404).json({ message: 'user not found!' });
    }
    //unhash password
    bcrypt.compare(req.body.password, user.password, async (err, result) => {
      if (result) {
        //generate token
        const accessToken = await jwt.sign(
          { _id: user._id.toString() },
          process.env.JWT_SECRET_KEY
        );
        // user.tokens = user.tokens.concat({ token: accessToken });
        //save token
        user.save().then(() => {
          res.cookie('access_token', `Bearer ${accessToken}`);
          res.json({ user, token: accessToken });
        });
        return;
      }
      res.status(400).json({ message: 'password not match!' });
    });
  });
};

const GetUser = async (req, res) => {
  const user = await User.findById(req.userId);
  if (!user) {
    return res.status(404).json({ message: 'user not found!' });
  }
  delete user.password;

  res.status(200).json(user);
};

const updateUser = async (req, res) => {
  const user = await User.findById(req.userId);
  if (!user) {
    return res.status(404).json({ message: 'user not found!' });
  }

  User.findByIdAndUpdate(
    { _id: req.userId },
    req.body,
    { new: true },
    (err, user) => {
      if (err) {
        return res.status(500).json({ message: err });
      }
      res.status(200).json(user);
    }
  );
};

const deleteUser = async (req, res) => {
  const user = await User.findById(req.userId);
  if (!user) {
    return res.status(404).json({ message: 'user not found!' });
  }
  user.remove().then(() => {
    res.status(200).json({ message: 'user deleted!' });
  });
};

/************cloudinary config*************/

/************cloudinary config*************/

/************start user register*************/
module.exports = {
  Inscription,
  Authentification,
  GetUser,
  updateUser,
  deleteUser,
};
