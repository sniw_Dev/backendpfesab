/** @format */

const colis = require('../models/colis.model');

const Create_colis = (req, res) => {
  const new_colis = new colis(req.body);
  new_colis
    .save()
    .then((colis) => {
      console.log(colis);
      res.status(200).send({ message: 'Colis created successfully!' });
    })
    .catch((err) => {
      res.status(500).send({ message: err });
    });
};

const Get_colis = (req, res) => {
  colis.find({ user_id: req.params.id }, (err, colis) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    res.status(200).send(colis);
  });
};

const Edit_colis = (req, res) => {
  console.log(req.body);
  colis.findByIdAndUpdate(
    req.params.id,
    req.body,
    { new: true },
    (err, colis) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      } else if (colis) {
        res.status(200).send({ message: 'Colis updated successfully!', colis });
      }
    }
  );
};

const Delete_colis = (req, res) => {
  colis.findByIdAndRemove(req.params.id, (err, colis) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    res.status(200).send({ message: 'Colis deleted successfully!' });
  });
};

module.exports = {
  Delete_colis,
  Edit_colis,
  Get_colis,
  Create_colis,
};
