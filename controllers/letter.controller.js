/** @format */

const letter = require('../models/lettre.model');

const Create_letter = (req, res) => {
  const new_letter = new letter(req.body);
  new_letter
    .save()
    .then((letter) => {
      console.log(letter);
      res.status(200).send({ message: 'Letter created successfully!' });
    })
    .catch((err) => {
      res.status(500).send({ message: err });
    });
};

const Get_letter = (req, res) => {
  letter
    .find({ user_id: req.params.id })
    .limit(30)
    .then((letters) => {
      res.status(200).send(letters);
    })
    .catch((err) => {
      res.status(500).send({ message: err });
    });
};

const Edit_letter = (req, res) => {
  letter.findByIdAndUpdate(
    req.params.id,
    req.body,
    { new: true },
    (err, letter) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      res.status(200).send({ message: 'Letter updated successfully!' });
    }
  );
};

const Delete_letter = (req, res) => {
  letter.findByIdAndRemove(req.params.id, (err, letter) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    res.status(200).send({ message: 'Letter deleted successfully!' });
  });
};

module.exports = {
  Delete_letter,
  Edit_letter,
  Get_letter,
  Create_letter,
};
