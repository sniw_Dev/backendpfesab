/** @format */

const rendezvous = require('../models/rendez_vous.model');

const Create_rendezvous = (req, res) => {
  const new_rendezvous = new rendezvous(req.body);
  new_rendezvous
    .save()
    .then((rendezvous) => {
      console.log(rendezvous);
      res.status(200).send({ message: 'Rendez-vous created successfully!' });
    })
    .catch((err) => {
      res.status(500).send({ message: err });
    });
};

const Get_rendezvous = (req, res) => {
  rendezvous.find({ user_id: req.params.id }, (err, rendez_vous) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    res.status(200).send({ rendez_vous });
  });
};

const Edit_rendezvous = (req, res) => {
  rendezvous.findByIdAndUpdate(
    req.params.id,
    req.body,
    { new: true },
    (err, rendezvous) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      res.status(200).send({ message: 'Rendez-vous updated successfully!' });
    }
  );
};

const Delete_rendezvous = (req, res) => {
  rendezvous.findByIdAndRemove(req.params.id, (err, rendezvous) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    res.status(200).send({ message: 'Rendez-vous deleted successfully!' });
  });
};

const Get_all_rendezvous = (req, res) => {
  rendezvous
    .find({ user_id: req.params.id })
    .sort({ _id: -1 })
    .then((rendez_vous) => {
      res.status(200).send({ rendez_vous });
    })
    .catch((err) => {
      res.status(500).send({ message: err });
    });
};

module.exports = {
  Create_rendezvous,
  Get_rendezvous,
  Edit_rendezvous,
  Delete_rendezvous,
  Get_all_rendezvous,
};
